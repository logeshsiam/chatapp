import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import database from '@react-native-firebase/database';

let itemsRef = database().ref('/users');

const Page2 = ({navigation}) => {
  const DATA = [
    {
      name: '1',
      image: require('../../assets/dp2.jpg'),
      title: 'Alex Hill',
      status: 'Hey! Coffee at noon?',
      done: '3 done',
      type: 0,
    },
    {
      name: '2',
      title: 'Kelly Baker',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
    {
      name: '3',
      title: 'Laura Dexter',
      image: require('../../assets/dp2.jpg'),
      status: 'Hey! Coffee at noon?',
      type: 2,
    },

    {
      name: '4',
      title: 'Pam Abel',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 3,
    },
    {
      name: '5',
      title: 'Kevin Lee',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 0,
    },
    {
      name: '6',
      title: 'Thomas Otto',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
    {
      name: '7',
      title: 'Stephanie Weber',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 0,
    },
    {
      name: '8',
      title: 'Winston Stewart',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
    {
      name: '1',
      image: require('../../assets/dp2.jpg'),
      title: 'Alex Hill',
      status: 'Hey! Coffee at noon?',
      done: '3 done',
      type: 0,
    },
    {
      name: '2',
      title: 'Kelly Baker',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
    {
      name: '3',
      title: 'Laura Dexter',
      image: require('../../assets/dp2.jpg'),
      status: 'Hey! Coffee at noon?',
      type: 2,
    },

    {
      name: '4',
      title: 'Pam Abel',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 3,
    },
    {
      name: '5',
      title: 'Kevin Lee',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 0,
    },
    {
      name: '6',
      title: 'Thomas Otto',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
    {
      name: '7',
      title: 'Stephanie Weber',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 0,
    },
    {
      name: '8',
      title: 'Winston Stewart',
      status: 'Hey! Coffee at noon?',
      image: require('../../assets/dp2.jpg'),
      type: 1,
    },
  ];



  const [itemsArray, setItemsArray] = React.useState([]);
  React.useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      const users = Object.values(data);
      setItemsArray(users);
    });
  }, []);


  return (
    <SafeAreaView style={{flex: 1}}>
      {/* hEader*/}
      <View style={{flexDirection: 'row', backgroundColor: '#3b95ff'}}>
        <View style={{flex: 1, height: 50}}>
          <Text 
          onPress={() => navigation.navigate('Page3')}
          style={styles.messages}> Messages</Text>
        </View>
      </View>
      {/* boxes */}
      <View >
        <FlatList
          data={itemsArray}
          renderItem={({item}) => (
            <SafeAreaView style={styles.container}>
              <View 
              
              style={{flex: 0.25}}>
                <View
                  style={{
                    height: 60,
                    width: 60,
                    borderRadius: 30,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    textAlign: 'center',
                  }}>
                  <Image style={styles.icons} source={item.image} />
                </View>
              </View>

              <View
                style={{
                  flex: 0.8,
                  height: 70,
                  flexDirection: 'column',
                  borderBottomWidth: 0.5,
                  borderBottomColor: '#c8c8c8',
                }}>
                <Text style={{fontSize: 22, fontWeight: '500', color: '#000'}}>
                  {item.name}
                </Text>
                {/* <Text style={{fontSize: 16, marginTop: 5}}>{item.status}</Text> */}
              </View>
            </SafeAreaView>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  messages: {
    fontSize: 28,
    fontWeight: '500',
    marginLeft: 10,
    color: '#fff',
  },
  icons: {
    alignSelf: 'center',
    height: 60,
    width: 60,
    borderRadius: 30,
  },
});

export default Page2;
