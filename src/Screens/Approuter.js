import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootStackScreen from './RootStackScreen';
const Approuter = () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default Approuter;
