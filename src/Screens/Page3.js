import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
} from 'react-native';
import AIcon from 'react-native-vector-icons/AntDesign';
import FIcon from 'react-native-vector-icons/FontAwesome';

const Page3 = ({navigation}) => {
  const DATA = [
    {
      text: 'Great seeing you today !',
      user: true,
    },
    {
      text: 'Likewise !',
      user: false,
    },
    {
      text: 'Hey! Coffee at noon ?',
      user: false,
    },
    {
      text: 'Sorry for the late response...How about 2:30 PM?',
      user: true,
    },
    {
      text: 'Likewise !',
      user: false,
    },

    {
      text: 'Hey! Coffee at noon ?',
      user: false,
    },
    {
      text: 'Sorry for the late response...How about 2:30 PM?',
      user: true,
    },
    {
      text: 'Likewise !',
      user: false,
    },
    {
      text: 'Hey! Coffee at noon ?',
      user: false,
    },
    {
      text: 'Sorry for the late response...How about 2:30 PM?',
      user: true,
    },
  ];

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* hEader*/}
      <View style={styles.body}>
        <View
          style={{
            flex: 0.15,
            justifyContent: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('Page1')}>
            <AIcon
              name="arrowleft"
              size={26}
              color="#fff"
              style={{alignSelf: 'center'}}
            />
          </TouchableOpacity>
        </View>
        {/* photo */}
        <View
          style={{
            flex: 0.1,
            justifyContent: 'center',
          }}>
          <View
            style={{
              height: 45,
              width: 45,
              borderRadius: 20,
              alignSelf: 'center',
            }}>
            <Image
              style={styles.icons}
              source={require('../../assets/dp2.jpg')}
            />
          </View>
        </View>
        {/* profile */}
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            marginLeft: 20,
            justifyContent: 'center',
          }}>
          <Text style={styles.messages}> Alex Hill</Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#fff',
            }}>
            546-464-4647
          </Text>
        </View>
      </View>

      {/* boxes */}

      <FlatList
        data={DATA}
        renderItem={({item}) => (
          <View style={item.user ? styles.usersent : styles.userreceived}>
            <Text
              style={item.user ? styles.usersenttext : styles.userreceivedtext}>
              {item.text}
            </Text>
          </View>
        )}
      />

      {/* textinput */}

      <View
        style={{height: 60, flexDirection: 'row', justifyContent: 'center'}}>
        <TextInput style={styles.input} placeholder="Type Here" />

        <TouchableOpacity style={{flex: 0.2, justifyContent: 'center'}}>
          <FIcon
            name="send"
            size={28}
            color="#3b95ff"
            style={{marginLeft: 20}}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  body: {
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#3b95ff',
  },

  messages: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
  },
  icons: {
    alignSelf: 'center',
    height: 45,
    width: 45,
    borderRadius: 30,
  },

  usersent: {
    backgroundColor: '#3b95ff',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 20,
    marginVertical: 10,
    margin: 10,
    alignSelf: 'flex-end',
  },

  userreceived: {
    backgroundColor: '#eaeaea',
    paddingVertical: 9,
    paddingHorizontal: 15,
    borderRadius: 20,
    marginVertical: 8,
    margin: 10,
    alignSelf: 'flex-start',
  },

  usersenttext: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '500',
  },
  userreceivedtext: {
    color: '#000',
    fontSize: 14,
    fontWeight: '500',
  },
  input: {
    color: '#000',
    flex: 0.7,
    height: 40,
    borderWidth: 0.2,
    padding: 10,
    borderRadius: 30,
    alignSelf: 'center',
  },
});

export default Page3;
