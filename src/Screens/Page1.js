import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  Button,
  Alert,
} from 'react-native';
import database from '@react-native-firebase/database';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

const Page1 = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [Number, setNumber] = React.useState('');

  const submit = () => {
    if (name === '') {
      Alert.alert('Chat App', 'Enter the name');
    } else if (Number === '') {
      Alert.alert('Chat App', 'Enter the Phone No');
    } else {
      addItem(name);
      navigation.navigate('Page2');
      //Alert.alert('Item saved successfully');
    }
  };

  const moment = require('moment');
  const time = moment();
  let addItem = user => {
    database()
      .ref('/users')
      .push({
        name: user,
        image: require('../../assets/dp2.jpg'),
        unix: time.unix(),
        utc: time.format('YYYY-MM-DD HH:mm:ss'),
        uuid: uuidv4(),
      });
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: '#48c0e8',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <TextInput
        onChangeText={text => setName(text)}
        style={styles.input}
        placeholder="Enter Your Name"
      />
      <TextInput
        onChangeText={text => setNumber(text)}
        style={styles.input}
        placeholder="Enter Your Phone no"
        keyboardType={'phone-pad'}
        maximumLength={10}
      />
      <View style={{margin: 10, width: 130}}>
        <Button onPress={submit} title="Login" color="blue" />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  input: {
    color: '#000',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 300,
    margin: 10,
    borderWidth: 2,
    padding: 10,
    backgroundColor: '#fff',
  },
});
export default Page1;
