import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Page1 from './Page1';
import Page2 from './Page2';
import Page3 from './Page3';
const RootStack = createNativeStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator
    screenOptions={{headerShown: false}}
    initialRouteName="Page1">
    <RootStack.Screen name="Page1" component={Page1} />
    <RootStack.Screen name="Page2" component={Page2} />
    <RootStack.Screen name="Page3" component={Page3} />
  </RootStack.Navigator>
);
export default RootStackScreen;
