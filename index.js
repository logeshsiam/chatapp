/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Page1 from './src/Screens/Page1';
import Page2 from './src/Screens/Page2';
import Page3 from './src/Screens/Page3';
import Approuter from './src/Screens/Approuter';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Approuter);
